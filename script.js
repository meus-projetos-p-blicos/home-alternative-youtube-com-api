// Substitua a chave de API pela sua chave
const API_KEY = "secret";

// IDs dos canais a serem buscados
const CHANNEL_IDS = [
  "UCWFmlmegFcu6lvAmeBtV3cg",
];

// Quantidade de vídeos a serem exibidos
const MAX_RESULTS = 5;
for (const channelId of CHANNEL_IDS) {

// URL da API do YouTube
const API_URL = `https://www.googleapis.com/youtube/v3/search?key=${API_KEY}&channelId=${channelId}&part=snippet,id&order=date&maxResults=${MAX_RESULTS}&publishedAfter=2022-01-01T00:00:00Z`;

// Função para adicionar vídeos à lista
function addVideoToList(video) {
	const { id, snippet } = video;
	const { videoId } = id;
	const { title, thumbnails } = snippet;
	const { url } = thumbnails.medium;
/*
	const videoItem = document.createElement("div");

	const thumbnail = document.createElement("img");
	thumbnail.src = url;

	const link = document.createElement("a");
	link.href = `https://www.youtube.com/watch?v=${videoId}`;
	link.textContent = title;

	videoItem.appendChild(thumbnail);
	videoItem.appendChild(link);

	document.getElementById("video-list").appendChild(videoItem);
*/
  const videoList = document.getElementById('video-list-programacao');
  const divElement = document.createElement('div');
  divElement.className = 'col-12 col-lg-4 mb-2 mt-2';
  urlVideo = `https://www.youtube.com/watch?v=${videoId}`;
  console.log(url);
  // Definir a propriedade "innerHTML" do novo elemento
divElement.innerHTML = `
<div class="card">
    <img src="${url}" class="card-img-top" alt="...">
    <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <a href="${urlVideo}" class="btn btn-primary">Acessar</a>
    </div>
</div>
`;

// Adicionar o novo elemento à lista de filhos do elemento "video-list"
videoList.appendChild(divElement);
}

// Busca os vídeos do YouTube e adiciona à lista
fetch(API_URL)
	.then((response) => response.json())
	.then((data) => {
		const { items } = data;
		items.forEach((item) => {
			const { id } = item;
			if (id.kind === "youtube#video") {
				addVideoToList(item);
			}
		});
	})
	.catch((error) => {
		console.error(error);
		alert("Erro ao buscar vídeos do YouTube.");
	});
}